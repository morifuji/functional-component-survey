import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import "./registerServiceWorker";

import "bulma";

import VueKonva from "vue-konva";

import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(VueKonva);

Vue.use(BootstrapVue);

Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
